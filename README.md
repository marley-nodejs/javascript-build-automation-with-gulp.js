# JavaScript Build Automation With Gulp.js [ENG, 2015]

___


src: https://github.com/johnpapa/pluralsight-gulp

___


### 04 Getting Started With Gulp

Installing the Gulp CLI  

    npm install -g gulp
    npm install --save-dev gulp
    gulp wello-world


### 05 Code Analysis With JSHint and JSCS

05 04 Coding the JSHint and JSCS Task

    npm install --save-dev gulp-jshint
    npm install --save-dev jshint-stylish
    npm install --save-dev gulp-jscs

    gulp vet

05 05 Reusable Functions  

    npm install --save-dev gulp-util

05 06 Failing the Task


05 07 Conditionally Displaying the Source Files

    npm install --save-dev gulp-print
    npm install --save-dev gulp-if
    npm install --save-dev yargs

    gulp vet --verbose


05 08 Lazy Loading Gulp Plugins

    npm install --save-dev gulp-load-plugins

    gulp vet --verbose


05 09 Reusable Configuration Module

    gulp vet --verbose



### 06 CSS Compilation  

06 03 Creating a Less and AutoPrefixer Gulp Task  


    npm install --save-dev gulp-less
    npm install --save-dev gulp-autoprefixer

    gulp styles


06 04 Deleting Files in a Dependency Task


    npm install --save-dev del
    gulp clean-styles
    gulp styles


06 05 Creating a Watch Task to Compile CSS


    gulp less-watcher


06 06 Handling Errors and Using Gulp Plumber

    npm install --save-dev gulp-plumber
    gulp less-watcher



### 07 HTML Injection

07 03 Adding Bower Files and Your JavaScript to the HTML

    npm install --save-dev wiredep
    npm install --save-dev gulp-inject


07 04 Removing Scripts and Styles From the Main HTML

    gulp wiredep


07 05 Adding Bower Files Automatically on Install

    bower install --save angular-resource
    bower uninstall --save angular-resource


07 06 Injecting Custom CSS

    gulp inject
    nodemon src/server/app.js


### 08 Serving Your Dev Build

08 03 Prepare, Serve, and Restart the Code


    npm install --save-dev gulp-nodemon
    gulp serve-dev


08 04 Run Tasks on Node Restart

    gulp serve-dev



### 09 Keeping Your Browser in Sync

09 03 Configuring browser-sync


    npm install --save-dev browser-sync
    gulp serve-dev

    http://localhost:3000/


09 04 Injecting CSS From Less

    http://localhost:3000/


09 05 Connecting browser-sync and nodemon  


09 06 Synchronizing Multiple Browsers

    gulp serve-dev --nosync
    gulp serve-dev



### 10 Building Assets and Keeping Organized

10 03 Creating Task Listing


    npm install --save-dev gulp-task-listing
    npm install --save-dev gulp-imagemin
    gulp help
    gulp


10 04 Copying Fonts

    gulp fonts


10 05 Optimizing Images

    gulp images


10 06 Cleaning


    gulp clean-fonts
    gulp clean-images
    gulp clean-slyles
    gulp clean

    gulp fonts
    gulp images
    gulp styles


### 11 Caching HTML Templates for Angular

11 03 Cleaning the Built Code Folder

    gulp clean-code


11 04 Minifying HTML and Putting in $templateCache


    npm install --save-dev gulp-minify-html
    npm install --save-dev gulp-angular-templatecache

    gulp templatecache



### 12 Creating a Production Build Pipeline


12 03 Creating the Optimize Gulp Task With Template Cache

    gulp optimize


12 04 Adding gulp-useref to the Optimization Pipeline

    npm install --save-dev gulp-useref

    gulp optimize


12 05 Cleaning and Serving the Built Code

    NODE_ENV=build node src/server/app.js


12 06 Serving the Optimized Build

    gulp fonts
    gulp images
    gulp styles

    gulp optimize
    gulp serve-dev
    gulp serve-build


### 13 Minifying and Filtering

13 03 Optimizing CSS

    npm install --save-dev gulp-filter
    npm install --save-dev gulp-csso
    npm install --save-dev gulp-uglify

    gulp clean
    gulp optimize


13 04 Optimizing JavaScript


    gulp clean
    gulp optimize


13 05 Serving Optimized Code

    gulp serve-build

http://localhost:3000/


13 06 When Optimized Code Fails

Добавили angular директиву ng-strict-di  в index.html

    gulp serve-build

http://localhost:3000/


### 14 Angular Dependency Injections


14 03 Adding ng-annotate to the Optimization Task


    npm install --save-dev gulp-ng-annotate

    gulp optimize


14 04 Adding Hints


### 15 Static Asset Revisions and Version Bumping


15 03 Adding Static Asset Revisions and Replacements


    npm install --save-dev gulp-rev
    npm install --save-dev gulp-rev-replace


    gulp clean
    gulp optimize


15 04 Generating a Revision Manifest

    gulp clean
    gulp optimize


15 05 Bumping Versions With Server

    npm install --save-dev gulp-bump

    gulp bump --type=minor
    gulp bump --version=2.3.4
    gulp bump --version=0.1.0


### 16 Testing

16 03 Creating the First Test Task


16 04 Karma Configuration  


16 05 Installing Packages and Running the Tests


    npm install --save-dev karma
    npm install --save-dev karma-chai
    npm install --save-dev karma-chai-sinon
    npm install --save-dev karma-coverage
    npm install --save-dev karma-growl-reporter
    npm install --save-dev karma-mocha
    npm install --save-dev karma-phantomjs-launcher
    npm install --save-dev karma-sinon
    npm install --save-dev mocha
    npm install --save-dev mocha-clean
    npm install --save-dev sinon-chai
    npm install --save-dev sinon
    npm install --save-dev phantomjs

    gulp test


16 06 Making Tests Run Before Other Tasks

    npm install --save-dev lodash
    npm install --save-dev node-notifier

    gulp serve-build


16 07 Continuously Running Tests During Development

    gulp autotest


### 17 Integration Testing and HTML Test Runners

17 03 Running Tests That Require a Node Server

    gulp test
    gulp test --startServers


17 04 Setting Up an HTML Test Runner Task

17 05 Injecting the HTML

17 06 browser-sync and the Test Runner

17 07 Launching the HTML Test Runner

    gulp serve-specs

http://localhost:3000/specs.html


17 08 Running Server Tests in the HTML Test Runner

    gulp serve-specs
    gulp serve-specs --startServers



### 18 Migrating to Gulp 4

18 03 Migrating

    npm install -g git://github.com/gulpjs/gulp#4.0
    npm install --save-dev git://github.com/gulpjs/gulp#4.0

    gulp -v

> CLI version 0.1.4  
> Local version 4.0.0-alpha.1

    gulp vet

18 04 Running the Refactored Tasks

    gulp --tasks-simple
    gulp clean
    gulp serve-dev
    gulp test
    gulp serve-specs

http://localhost:3000/specs.html

    gulp build
    gulp --tasks


18 05 Installing the Latest Gulp

    npm uninstall -g gulp-cli
    npm uninstall -g gulp
    npm uninstall --save-dev gulp

    gulp -v

    npm install -g gulp
    npm install --save-dev gulp
